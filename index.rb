require 'sinatra'
require 'json'

users = [
  {id: 1, name: "Usuario 1"},
  {id: 2, name: "Usuario 2"}
]

get '/users' do
  return users.to_json
end

get '/users/:id' do
  id = params["id"].to_i
  return users[id - 1].to_json
end

post '/users' do
  new_user = {id: users.length + 1, name: params["name"]}   
  users << new_user
  return new_user.to_json
end

put '/users/:id' do
  id = params["id"].to_i
  updated_user = {id: id, name: params["name"]}
  users[id - 1] = updated_user
  return updated_user.to_json
end

delete '/users/:id' do
  id = params["id"].to_i
  users.delete_at(id - 1)
  return {message: "Usuario eliminado con éxito"}.to_json
end

set :port, 4000
run Sinatra::Application.run!       