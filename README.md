# Project Title
Actividad| Aprendiendo Micro web frameworks

## Getting Started/ Installing

Iniciamos la ejecución con: npm test ó ruby index.js

### Prerequisites

Instalar ruby:  sudo apt-get update && sudo apt-get install -y ruby-full build-essential 
Instalar Sinatra: sudo gem install sinatra

### Testing
Post:localhost:4000/users?name=Luis
Get:localhost:4000/users/3
Get:localhost:4000/usersPut:
localhost:4000/users/1?name=MarcosDelete:
localhost:4000/users/2

### And coding style tests

Estilo de escritura recomendado para Ruby.

## Built With

* Ruby 
* Sinatra 

## Versioning

v1.0

## Authors

* Fátima Monserrath Duarte Pérez 353324
* América Guadalupe Martínez Cano 348810
* Diego Alejandro Martínez González 353198
* Marcos Alfredo Aguilar Mata 353223

## License

Sin licencia.
